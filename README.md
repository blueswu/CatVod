# CatVod

基于vue和express的练手项目，实现聚合爬取解析流行影视网站苹果CMS的视频信息，可以用来自建简单的切片网站。
纯练手项目，请勿用于非法用途。

**AndroidTv端也已完成，暂未开源。**

前端采用vue开发。

后端使用express开发。

在线预览地址：[https://vod.ywy.me](https://vod.ywy.me) 测试账号：`demo@x.cn` 密码 `demoxx`

```
client: 前端
server：后端
android： android tv端


cd client 
npm install 
npm run serve

cd server 
npm install 
npm start

```

---

默认管理员 账号 `admin@x.cn` 密码 `adminxx`

---
![androidtv](./screenshot/androidtv2.jpg)
![androidtv](./screenshot/androidtv1.png)
![mobile](./screenshot/mobile.jpg)
![admin](./screenshot/admin.png)
![home](./screenshot/home.png)
![detail](./screenshot/detail.png)
---

## 已实现功能

### 用户模块
- 注册
- 登录
- 信息修改
- 观看历史

### 管理模块
- 系统设置
- 用户管理
- 分类管理
- 采集站管理

### 视频模块
- 采集
- 定时采集当天
- 视频分类
- 视频播放
- 视频搜索

## 待实现功能

- 暂无

---

### 前端 Vue

- `axios` 基于 promise 的 HTTP 库

- `element-ui` 基于Vue的前端组件库

- `vue-router`

- `vuex`

### 后端 express

- `sequelize` 基于promise的数据库ORM

- `fast-xml-parser` xml解析

- `jsonwebtoken` token验证

- `cors` 跨域请求工具

- `joi` 数据模型验证

- `moment` 日期时间处理

- `morgan` 日志中间件

- `lodash` JS工具库

- `node-schedule` 计划任务

- `nodemailer` 发送邮件

- `pinyin4js` 中文转拼音

- `segment` 中文分词

### 数据库 mysql