let config = require('../config');

module.exports = {
    index(req, res, next) {
        res.render('index', {title: config.system.appName});
    },

    /**
     * 获取app基本信息
     * @param req
     * @param res
     * @param next
     * @returns {Promise<void>}
     */
    info(req, res, next) {
        res.jsonSuccess({
            appName: config.system.appName,
        });
    },
};
