const indexController = require('../controller/index.controller');
const userController = require('../controller/user.controller');
const settingController = require('../controller/setting.controller');
const classController = require('../controller/class.controller');
const siteController = require('../controller/site.controller');
const videoController = require('../controller/video.controller');

module.exports = (app) => {
    // 权限拦截器
    app.all('/*', userController.auth);
    // helloworld
    app.get('/', indexController.index);
    // 获取站点基本信息
    app.all('/info', indexController.info);
    // 获取分类信息
    app.post('/classes', classController.classes);
    // 获取视频信息
    app.post('/videos', videoController.videos);
    // 获取某个视频信息
    app.post('/video/detail', videoController.detail);
    // 搜索视频
    app.post('/video/search', videoController.search);
    // 获取视频播放信息
    app.post('/video/play', videoController.play);
    // 用户信息
    app.post('/user/info', userController.info);
    // 用户观看历史
    app.post('/user/history', userController.history);
    // 用户观看历史
    app.post('/user/history/del', userController.historyDel);
    // 更新用户信息
    app.post('/user/update', userController.update);
    // 用户注册
    app.post('/user/reg', userController.reg);
    // 用户登录
    app.post('/user/login', userController.login);
    // 重发验证码
    app.post('/user/resendCode', userController.resendCode);
    // 用户列表
    app.post('/admin/users', userController.users);
    // 更新用户角色
    app.post('/admin/user/role', userController.userRole);
    // 启用禁用角色
    app.post('/admin/user/block', userController.userBlock);
    // 删除用户
    app.post('/admin/user/del', userController.userDel);
    // 设置列表
    app.post('/admin/settings', settingController.settings);
    // 增加设置
    app.post('/admin/setting/add', settingController.add);
    // 删除设置
    app.post('/admin/setting/del', settingController.del);
    // 更新设置
    app.post('/admin/setting/update', settingController.update);
    // 所有分类
    app.post('/admin/classes', classController.adminClasses);
    // 分类管理
    app.post('/admin/class/add', classController.add);
    // 分类修改
    app.post('/admin/class/update', classController.update);
    // 所有站点
    app.post('/admin/sites', siteController.sites);
    // 增加站点
    app.post('/admin/site/add', siteController.add);
    // 更新站点
    app.post('/admin/site/update', siteController.update);
    // 获取采集站点分类信息
    app.post('/admin/site/classes', siteController.classes);
    // 采集
    app.post('/admin/site/cj', siteController.cj);
    // 采集所有
    app.post('/admin/site/cjAll', siteController.cjAll);
};
