let _ = require('lodash');

/**
 * 开发配置
 * @type {{}}
 */
module.exports = {
    // 端口
    port: 20000,
    // 跨域
    whitelist: ['http://127.0.0.1:20000', 'http://localhost:8080', 'http://10.80.7.156:8080'],
    // 数据库
    db: {
        host: 'localhost',  // 数据库地址
        port: '3306',
        name: 'cat_vod',    // 数据库
        user: 'root',       // 数据库用户名
        pwd: 'sa',          // 密码
        pool: {             // 连接池设置
            max: 5,         // 最大连接数
            min: 0,         // 最小连接数
            idle: 10000,
        },
    },
    // 邮箱发送配置
    mail: {
        from: '"' + +'" <xxxx@qq.com>',
        smtp: {
            host: 'smtp.qq.com',
            port: 465,
            logger: true,
            auth: {
                user: 'xxxx@qq.com',
                pass: 'xxxx',
            },
        },
    },
    // token 秘钥
    tokenSecret: 'xxxxxxxxxxxxxxxx',
    // 采集代理
	spiderProxy: null,
    // spiderProxy: {
    //     host: '127.0.0.1',
    //     port: 8008,
    //     auth: {
    //         username: 'xx',
    //         password: 'xx',
    //     },
    // },
};
