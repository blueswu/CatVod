import Api from './api';

export default {
    users(pg, pgSize) {
        return Api.post('admin/users', {page: pg, pageSize: pgSize});
    },

    userRole(uid, role) {
        return Api.post('admin/user/role', {uid: uid, role: role});
    },

    userBlock(uid) {
        return Api.post('admin/user/block', {uid: uid});
    },

    userDel(uid) {
        return Api.post('admin/user/del', {uid: uid});
    },

    settings() {
        return Api.post('admin/settings');
    },

    settingAdd(data) {
        return Api.post('admin/setting/add', data);
    },

    settingDel(key) {
        return Api.post('admin/setting/del', {key: key});
    },

    settingUpdate(data) {
        return Api.post('admin/setting/update', data);
    },

    classes() {
        return Api.post('admin/classes');
    },

    classUpdate(data) {
        return Api.post('admin/class/update', data);
    },

    classAdd(data) {
        return Api.post('admin/class/add', data);
    },

    sites() {
        return Api.post('admin/sites');
    },

    siteAdd(data) {
        return Api.post('admin/site/add', data);
    },

    siteUpdate(data) {
        return Api.post('admin/site/update', data);
    },

    siteClasses(flag) {
        return Api.post('admin/site/classes', {flag: flag});
    },

    siteCJ(flag, hour, page) {
        return Api.post('admin/site/cj', {flag: flag, hour: hour, page: page});
    },

    cjAllSite() {
        return Api.post('admin/site/cjAll');
    },
};
