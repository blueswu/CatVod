import Api from './api';

export default {
    info() {
        return Api.post('user/info');
    },

    update(form) {
        return Api.post('user/update', form);
    },

    reg(form) {
        return Api.post('user/reg', form);
    },

    login(form) {
        return Api.post('user/login', form);
    },

    resendCode(form) {
        return Api.post('user/resendCode', form);
    },

    history() {
        return Api.post('user/history');
    },
    historyDel(id) {
        return Api.post('user/history/del', {id: id});
    },
};
