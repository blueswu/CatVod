import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Detail from '../views/Detail.vue';
import Search from '../views/Search.vue';

import store from '../store';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            title: '首页',
        },
    },
    {
        path: '/detail',
        name: 'detail',
        component: Detail,
        meta: {
            title: '详情',
        },
    },
    {
        path: '/search',
        name: 'search',
        component: Search,
        meta: {
            title: '搜索',
        },
    },
    {
        path: '/reg',
        name: 'reg',
        meta: {
            title: '注册',
        },
        component: () => import('../views/Reg.vue'),
    },
    {
        path: '/user',
        name: 'user',
        meta: {
            title: '用户信息',
        },
        component: () => import('../views/User.vue'),
    }, {
        path: '/admin',
        name: 'admin',
        meta: {
            title: '系统管理',
        },
        component: () => import('../views/Admin.vue'),
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            title: '登录',
        },
        component: () => import('../views/Login.vue'),
    },
    {
        path: '/spider',
        name: 'spider',
        meta: {
            title: '采集',
        },
        component: () => import('../views/Spider.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title + (store.state.appName ? (' - ' + store.state.appName) : '');
    }
    next();
});

export default router;
